import AbstractModel from 'app/abstract/Model'
import FilesSchema from './schema'
import fs from 'fs'

module.exports = new class extends AbstractModel {
  constructor () {
    super('Files Model')
  }

  getImages (resolve, reject) {
    FilesSchema.find({}, ['path', 'comment'], (error, result) => {
      if (error) return reject(error)
      resolve(result)
    })
  }

  getImagesByEmail (resolve, reject, email) {
    FilesSchema.find({userEmail: email}, ['path', 'comment'], (error, result) => {
      console.log(result)
      if (error) return reject(error)
      resolve(result)
    })
  }

  removeImage (resolve, reject, id, email) {
    id = this.mongoose.Types.ObjectId(id)
    let where = {_id: id}
    if (email) where.userEmail = email
    FilesSchema.findOne(where, (error, result) => {
      if (error || !result) return reject()
      result.remove((error) => {
        if(error) reject(error)
        else resolve()
      })
    })
  }

  saveFiles (resolve, reject, arrImages, userEmail) {
    let Files = []
    for (let key in arrImages) {
      const value = arrImages[key]
      fs.rename(value.path, 'public/upload/' + userEmail + '_' + value.originalFilename, (err) => {})
      Files.push({
        filename: value.originalFilename,
        type: value.type,
        userEmail: userEmail,
        path: 'upload/' + userEmail + '_' + value.originalFilename,
        comment: value.comment || ''
      })
    }

    FilesSchema.insertMany(Files, (error) => {
      if (error) return reject()
      resolve()
    });

  }
}
