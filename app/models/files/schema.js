import md5 from 'md5'
import mongoose from 'app/systems/mongoose'

let Schema = mongoose.Schema

let filesSchema = new Schema({

  filename: String,

  type: {
    type: String,
    enum: ['image/jpeg']
  },

  path: {
    type: String,
    unique: true,
    required: true
  },

  comment: {
    type: String
  },

  userEmail: {
    type: String,
    required: true
  },

  createdAt: {
    type: Date,
    default: Date.now
  }

})

filesSchema.pre('save', (next) => {
  if (this.comment.length > 150) {
    this.comment = this.comment.splice(0, 150)
  }
  next()
})



module.exports = mongoose.model('files', filesSchema)
