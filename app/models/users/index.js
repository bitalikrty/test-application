import AbstractModel from 'app/abstract/Model'
import UsersSchema from './schema'

module.exports = new class extends AbstractModel {
  constructor () {
    super('Users Model')
  }

  newUser (resolve, reject, user) {
    let User = new UsersSchema({
      name: user.name,
      email: user.email,
      password: user.password,
      role: user.role
    })
    User.save((error, result) => {
      if (error) {
        this.log(error)
        reject()
      }
      resolve(result)
    })
  }

  getUser (resolve, reject, userEmail, isFull) {
    let select = ['name', 'email', 'role']
    if (isFull) select.push('password')
    UsersSchema.findOne({email: userEmail, active: true}, select, (error, result) => {
      if (error) {
        this.log(error)
        reject()
      }

      resolve(result)
    })
  }
}
