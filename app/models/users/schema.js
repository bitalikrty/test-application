import md5 from 'md5'
import mongoose from 'app/systems/mongoose'

let Schema = mongoose.Schema

let usersSchema = new Schema({

  name: {
    type: String,
    required: true
  },

  email: {
    type: String,
    required: true,
    unique: true
  },

  password: {
    type: String,
    required: true
  },

  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user'
  },

  createdAt: {
    type: Date,
    default: Date.now
  },

  updateAt: {
    type: Date,
    default: Date.now
  },

  deleteAt: {
    type: Date
  },

  active: {
    type: Boolean,
    default: true
  }


})


usersSchema.pre('save', function (next) {
  if (!this.isModified('password')) next()
  if (!this.password || this.password === '') next()

  this.password = md5(this.password)
  next()
})

usersSchema.methods.comparePassword = function (password, next) {

  if (md5(password) === this.password) {
    next(null, true)
  } else {
    next(true)
  }
}

let users = mongoose.model('users', usersSchema)

export default users
