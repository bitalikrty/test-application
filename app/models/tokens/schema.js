import md5 from 'md5'
import mongoose from 'app/systems/mongoose'

let Schema = mongoose.Schema

let tokensSchema = new Schema({

  token: String,

  createdAt: {
    type: Date,
    default: Date.now
  }

})



module.exports = mongoose.model('tokens', tokensSchema)
