import AbstractModel from 'app/abstract/Model'
import TokensSchema from './schema'

module.exports = new class extends AbstractModel {
  constructor () {
    super('Tokens Model')
  }

  addToken (resolve, reject, token) {
    let newToken = new TokensSchema({
      token: token
    })

    newToken.save((error, result) => {
      if (error) return reject(error)
      resolve(result)
    })
  }

  getValidToken (resolve, reject) {
    TokensSchema.findOne().sort({createdAt: -1}).exec((error, result) => {
      if (error || !result) return reject(error)

      let date = Date.parse(result.createdAt)
      let nowDate = Date.now()
      let interval = this.getCf('adminTokenTime') * 60 * 1000

      if (date + interval > nowDate) {
        resolve(result.token)
      } else {
        reject()
      }
    })
  }


}
