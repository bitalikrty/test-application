let cf = {}

cf.jwt = 'very_1s/cr9et_to2c5e5n-key'
cf.mongooseConnect = 'mongodb://localhost:27017/application'
cf.adminTokenKey = 'secretWord'
cf.adminTokenTime = 10

module.exports = cf
