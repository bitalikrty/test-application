import MainClass from 'app/abstract/Main'
import Users from 'app/models/users'

module.exports = new class extends MainClass{
  constructor() {
    super('Users Controller')
  }

  get (req, res) {
    Users.$('getUser', [req.user.email])
    .then((user) => {
      res.send(200, user)
    })
  }


}
