import controller from './index.js'

module.exports = (app) => {

	app.get('/user', (req, res) => {
		controller.get(req, res)
	})

	app.put('/user/add-images', (req, res) => {
		controller.addImages(req, res)
	})
}
