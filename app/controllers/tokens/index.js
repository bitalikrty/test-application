import MainClass from 'app/abstract/Main'
import Tokens from 'app/models/tokens'
import randomstring from 'randomstring'

module.exports = new class extends MainClass{
  constructor() {
    super('Auth Controller')
  }

  get (req, res) {
    if (!req.body.password || (req.body.password !== this.getCf('adminTokenKey'))) {
      res.send(403)
      return false
    }
    let token = randomstring.generate()
    Tokens.$('addToken', [token])
    .then(() => {
      res.send(200, token)
    })
  }

}
