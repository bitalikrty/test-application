import controller from './index.js'
module.exports = (app) => {

	app.post('/auth', (req, res) => {
    controller.auth(req, res)
  })

  app.put('/register', (req, res) => {
    controller.register(req, res)
  })
}
