import MainClass from 'app/abstract/Main'

import passport from 'app/systems/passport'
import jwt from 'jsonwebtoken'

import Auth from 'app/modules/auth'
import Tokens from 'app/models/tokens'

module.exports = new class extends MainClass{
  constructor() {
    super('Auth Controller')
  }

  auth (req, res, next) {
    passport.authenticate('local', (err, token) => {
      if (err) return res.send(401)
      if (!token) {
        return res.status(401).json({ status: 'error', code: 'unauthorized' });
      } else {
        return res.send(200, token);
      }
    })(req, res, next);
  }

  register (req, res) {
    const payload = {
      name:     req.body.name,
      password: req.body.password,
      email:    req.body.email,
      role:     'user'
    }

    if (req.body.adminKey) {
      Tokens.$('getValidToken')
      .then((token) => {
        if (req.body.adminKey === token) {
          payload.role = 'admin'
          this.finishRegister(req, res, payload)
        } else {
          res.send(400, 'Token key is not valid')
          return false
        }
      })
      .catch((error) => {
        res.send(400, 'Token key is not valid')
      })
    } else {
      this.finishRegister(req, res, payload)
    }
  }

  finishRegister (req, res, payload) {
    Auth.$('register', [payload])
    .then((token) => {
      res.send(200, token)
    })
    .catch((userIsAlready) => {
      if(userIsAlready) res.send(422)
      else res.send(401)
    })
  }
}
