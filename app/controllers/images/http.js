import controller from './index.js'

module.exports = (app) => {

	app.put('/add-images', (req, res) => {
		controller.addImages(req, res)
	})

  app.get('/images', (req, res) => {
    controller.getImages(req, res)
  })

  app.delete('/delete-image', (req, res) => {
    controller.removeImage(req, res)
  })
}
