import MainClass from 'app/abstract/Main'
import Users from 'app/models/users'
import multiparty from 'multiparty'
import Files from 'app/models/files'

module.exports = new class extends MainClass{
  constructor() {
    super('Images Controller')
  }

  addImages (req, res) {

    let fields = req.body
    let files = req.files
    let arrImages = []
    let next = true

    if (!files) return res.send(400, 'Must have images')
    files.files.forEach((value) => {
      let fileType = value.headers['content-type']
      let fileName = value.originalFilename.split('.').reverse()
      fileName.splice(0, 1)
      fileName = fileName.reverse().join('.')
      if (fileType !== 'image/jpeg') next = false

      arrImages[value.originalFilename] = value
      arrImages[value.originalFilename].type = fileType
      arrImages[value.originalFilename].comment = fields.comment[fileName]
    })

    if (!next) return res.send(400, 'Some images has unacceptable type')

    Files.$('saveFiles', [arrImages, req.user.email])
    .then((result) => {
      res.send(200)
    })
    .catch((error) => {
      res.send(400, 'Some images already upload or images have wrong type')
    })
  }

  getImages (req, res) {
    Users.$('getUser', [req.user.email])
    .then((user) => {
      if(user.role == 'admin') {
        Files.$('getImages').then((images) => {
          res.send(200, this.preSendImages(req, images))
        })
      } else {
        Files.$('getImagesByEmail', [req.user.email]).then((images) => {
          res.send(200, this.preSendImages(req, images))
        })
      }
    })
  }

  removeImage (req, res) {
    Users.$('getUser', [req.user.email])
    .then((user) => {
      let email = null
      if(user.role == 'user') email = req.user.email

      Files.$('removeImage', [req.body.id, email])
      .then(() => res.send(200))
      .catch((error) => res.send(400, error) )

    })

  }

  preSendImages (req, images) {
    images.forEach((val, i) => {
      images[i]['path'] = req.protocol + '://' + req.get('host') + '/' + val.path
    })
    return images
  }

}
