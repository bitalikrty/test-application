module.exports = (app) => {

  require('app/controllers/auth/http')(app)
  require('app/controllers/tokens/http')(app)

  require('app/systems/authMiddlewares')(app)

  require('app/controllers/users/http')(app)
  require('app/controllers/images/http')(app)
}
