import MainClass from 'app/abstract/Main'
import md5 from 'md5'
import validator from 'validator'
import jwt from 'jsonwebtoken'
import Users from 'app/models/users'

const mod = new class extends MainClass {
	constructor() {
		super('Sign In')
	}

	auth (resolve, reject, payload) {
		Users.$('getUser', [payload.email, true])
    .then((user) => {
			user.comparePassword(payload.password, (error, result) => {
				if (error) {
					reject()
				} else {
					this.$('generateToken', [payload.email]).then(resolve)
				}
			})
    })
    .catch(reject)
	}

	register (resolve, reject, payload) {

		Users.$('getUser', [payload.email])
    .then((user) => {
			if (user) {
				reject(true)
				return false
			}

			if (!this.validRegister(payload)) {
				reject()
				return false
			}

			Users.$('newUser',[payload])
			.then((result) => {
				this.$('generateToken', [payload.email])
				.then(resolve)
			})
			.catch((error) => {
				this.log(error)
				reject()
			})

		})
	}

	validRegister (payload) {
		if (
			payload &&
			payload['name'] &&
			payload['email'] &&
			payload['password']
		){
			if (!validator.isEmail(payload['email'])) {
				return false
			}

			return true
		}
	}

	generateToken (resolve, reject, email) {
		jwt.sign(
			{
				email: email,
				exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 1000),
			}, this.getCf('jwt'), (err, token) => {
		        if(err)
		          reject(err)
		        else
		          resolve(token)
	      	}
		)
  }

	decodeToken (resolve, reject, token) {
    jwt.verify(token, this.getCf('jwt'), (err, decode) => {
      if(err)
        reject(err)
      else
        resolve(decode)
    })
  }


}

module.exports = mod
