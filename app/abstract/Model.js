import MainClass from 'app/abstract/Main'
import mongoose from 'mongoose'

module.exports = class extends MainClass {
  constructor (name) {
    super(name)
    this.mongoose = mongoose
  }

  // fetch (resolve, reject) {
  //   this.schema.find({}, (error, result) {
  //     if (error) {
  //       this.log(error)
  //       reject(error)
  //     }
  //
  //     resolve(result)
  //   })
  // }

}
