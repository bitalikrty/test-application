import config from 'app/config'

module.exports = class {
  constructor (name) {
    this.name = name
    this._config = config
  }

  $ (actions, args) {
    if(!this[actions]){
      this.log('Actions ::' + actions + ' is not defined')
      return false
    }
    if(args)
      return new Promise((resolve, reject) => this[actions](resolve, reject, ...args))
    else
      return new Promise((resolve, reject) => this[actions](resolve, reject))
  }

  log (mss) {
    console.log('-----------------------')
    console.log(this.name + '::')
    console.log(mss)
    console.log('-----------------------')

  }

  getCf (param) {
    if (this._config && this._config[param]) {
      return this._config[param]
    }
  }



}
