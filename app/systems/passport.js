import passport from 'passport'
import LocalStrategy from 'passport-local'
import BearerStrategy from 'passport-http-bearer'
import jwt from 'jsonwebtoken'

import auth from 'app/modules/auth'
import config from 'app/config'

let localStrategy   = LocalStrategy.Strategy
let bearerStrategy  = BearerStrategy.Strategy

passport.use(new localStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, function(email, password, next) {
  auth.$('auth', [{email: email, password: password}])
  .then((token) => {
    next(null, token)
  })
  .catch((error) => {
    next(error)
  })
}));

passport.use(new bearerStrategy(function (token, next) {
  jwt.verify(token, config.jwt, function(err, decoded) {
    if (err) return next(err);
    return next(null, decoded.email);
  });
}));

passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

module.exports = passport
