import passport from 'app/systems/passport'

module.exports = (app) => {
  app.all('*', function(req, res, next) {
    passport.authenticate('bearer', function(err, email, info) {
      if (err) return next(err);
      if (email) {
        req.user = {email: email};
        return next();
      } else {
        return res.status(401).json({ status: 'error', code: 'unauthorized' });
      }
    })(req, res, next);
  })
}
