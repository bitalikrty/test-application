import mongoose from 'mongoose'
import config from 'app/config'

mongoose.connect(process.env.MONGODB_URI || config.mongooseConnect, {useNewUrlParser: true })

export default mongoose
